const mongoose = require(`mongoose`)

const orderSchema = new mongoose.Schema ({
    userId: {
        type: String,
        required: [true, `User ID is required`]
    },
    productOrdered: [{
        productId: {
            type: String,
            required: [true, `Product ID is required`]
        },
        name: {
            type: String,
            required: [true, `Name is required!`]
        },
        description: {
            type: String,
            required: [true, `Description is required!`]
        },
        price: {
            type: Number,
            required: [true, `Number is required!`]
        },
        quantity: {
            type: Number,
            required: [true, `Quantity is required!`]
        }
    }],
    totalAmount: {
        type: Number,
        required: [true, `Total amount is required`]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    orderStatus: {
        type: String,
        default: "Pending"
    }
}, {timestamps: true})

module.exports = mongoose.model(`Order`, orderSchema)

