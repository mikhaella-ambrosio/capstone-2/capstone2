const mongoose = require(`mongoose`)

const userSchema = new mongoose.Schema ({
    firstName: {
        type: String,
        required: [true, `First Name is required!`],
        unique: true
    },
    lastName: {
        type: String,
        required: [true, `Last Name is required!`],
        unique: true
    },
    email: {
        type: String,
        required: [true, `Email is required!`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password is required!`]
    },
    confirmPw: {
        type: String,
        required: [true, `Password is required!`]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orderedItems: [{
        productId: {
            type: String,
            required: [true, `Product ID is required.`]
        },
        productName: {
            type: String,
            required: [true, `Product name is required.`]
        },
        productQty: {
            type: Number,
            required: [true, `Ordered product quantity is required.`]
        },
        productPrice: {
            type: Number,
            required: [true, `Product price is required.`]
        },
        subTotal: {
            type: Number,
            required: [true, `Order subtotal is required.`]
        }
    }]
}, {timestamps: true})

module.exports = mongoose.model(`User`, userSchema)