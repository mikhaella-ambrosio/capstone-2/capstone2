const express = require(`express`);
const router = express.Router()

const {
    create,
    getAllProducts,
    getAProduct,
	getActiveProducts,
	getArchivedProducts,
    updateProduct,
    archive,
	unarchive,
	deleteProduct
} = require (`./../controllers/productControllers`)

const {verify, verifyAdmin} = require(`./../auth`)

// CREATE A NEW PRODUCT
router.post('/create', verifyAdmin, async (req, res) => {
	// console.log(req.body) 
	try{
		await create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// GET ALL PRODUCTS
router.get(`/allProducts`, async (req,res) => {
    try {
        await getAllProducts().then(result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

// GET A SPECIFIC PRODUCT
router.get('/:productId', verify, async (req, res) => {
	try{
		await getAProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// GET ACTIVE PRODUCTS
router.get('/activeProducts', verifyAdmin, async (req, res) => {
	try{
		await getActiveProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// GET ARCHIVE PRODUCTS
router.get('/isArchived', verifyAdmin, async (req, res) => {
	try{
		await getArchivedProducts().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

// UPDATE A SPECIFIC PRODUCT
router.put('/:productId/updateProduct', verifyAdmin, async (req, res) => {
    // console.log(req.params)
	try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// ARCHIVE A PRODUCT
router.patch('/:productId/archive', verifyAdmin, async (req, res) => {
	try{
		await archive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// UNARCHIVE A PRODUCT
router.patch('/:productId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unarchive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// DELETE A PRODUCT
router.delete(`/:productId/deleteProduct`, verifyAdmin, async (req,res) => {
    try {
        await deleteProduct(req.params.productId).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})


module.exports = router