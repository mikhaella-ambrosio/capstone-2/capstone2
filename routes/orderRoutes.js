const express = require(`express`);
const router = express.Router()

const {
    checkout,
    getAllOrders,
    getAllPending,
    getAllDelivered,
    getOrder,
    delivered,
    deleteOrder,
    addToCart
} = require (`./../controllers/orderControllers`)

const {verify, verifyAdmin, verifyUser, decode} = require(`./../auth`);
const Product = require("../models/Product");


// USER CHECKOUT
router.post('/checkout', verify, async(req, res) =>{
    const id = decode(req.headers.authorization)
    // console.log(decode(req.headers.authorization))  // id, email, isAdmin
    
    const data = {
        userId: id,
        productId: req.body.productId,
        name: req.body.name,
        price: req.body.price,
        quantity: req.body.quantity
    }
    // console.log(data) // userid, productid,quantity
    try{
        // console.log(`hello`)
        await checkout(data).then(result => {
            res.send(result)
            
        })
    }
    catch(err){
        res.status(500).json(err)
    }
})

// GET ALL ORDERS
router.get(`/allOrders`,verifyAdmin, async (req,res) => {
    try {
        await getAllOrders().then(result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

// GET ALL PENDING ORDERS
router.get(`/allPending`,verifyAdmin, async (req,res) => {
    try {
        await getAllPending().then(result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

// GET ALL DELIVERED ORDERS
router.get(`/allDelivered`,verifyAdmin, async (req,res) => {
    try {
        await getAllDelivered().then(result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

// GET USER'S ORDER/S
router.get('/:userId/getOrder', verify, async (req, res) => {
    const user = decode(req.headers.authorization).isAdmin
	if(user == false){
        try{
            await getOrder(req.params.userId).then(result => res.send(result))
    
        }catch(err){
            res.status(500).json(err)
        }
    } else{
		res.send(`Only with user access can their orders.`)
	}	
})

// UPDATE USER'S ORDERSTATUS STATUS TO DELIVERED
router.patch(`/delivered`, verifyAdmin, async (req, res) => {
    try {
        await delivered(req.body).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})

// DELETE A USER
router.delete(`/delete-order`, verifyAdmin, async (req,res) => {
    try {
        await deleteOrder(req.body).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})
// EXPORT ROUTES MODULE
module.exports = router