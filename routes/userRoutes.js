const express = require(`express`);
const router = express.Router()

const {
    register,
    getAllUsers,
    login,
    profile,
    profileUpdate,
    adminStatus,
    userStatus,
    deleteUser,
    checkout
} = require (`./../controllers/userControllers`)

const {verify, verifyAdmin, verifyUser, decode} = require(`./../auth`)

// REGISTER A USER
router.post(`/register`, async (req,res) => {
    try {
        await register(req.body).then(result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

// GET ALL USERS 
router.get(`/allUsers`, verifyAdmin, async (req,res) => {
    try {
        await getAllUsers().then(result => res.send(result))
    } catch(err) {
        res.status(500).json(err)
    }
})

// LOGIN A USER
router.post(`/login`, (req,res) => {
    // console.log(req.body)
	// const userId = decode(req.headers.authorization).id
    // console.log(req.headers)

    try {
        login(req.body).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})

// RETRIEVE USER INFO
router.get(`/profile`, verify, async (req, res) => {
	const userId = decode(req.headers.authorization).id
    console.log(userId)
    try {
        await profile(userId).then(result => res.send(result))
        
    } catch (err) {
    res.status(500).json(err)
}

})

// UPDATE USER INFO
router.put(`/profile-update`, verifyUser, async (req,res) => {
    try {
        await profileUpdate(req.body).then(result => res.send(result))
    }catch (err) {
        res.status(500).json(err)
    }
})

// UPDATE USER'S ISADMIN STATUS TO TRUE
router.patch(`/:userId/setAsAdmin`, verifyAdmin, async (req, res) => {
    try {
        await adminStatus(req.params.userId).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})

// UPDATE USER'S ISADMIN STATUS TO FALSE
router.patch(`/:userId/setAsUser`, verifyAdmin, async (req, res) => {
    try {
        await userStatus(req.params.userId).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})

// DELETE A USER
router.delete(`/:userId/delete-user`, verifyAdmin, async (req,res) => {
    try {
        await deleteUser(req.params.userId).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})

// EXPORT ROUTES MODULE
module.exports = router