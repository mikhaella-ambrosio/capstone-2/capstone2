const res = require("express/lib/response")
const { createToken } = require("../auth")
const User = require(`./../models/User`)
const CryptoJS = require("crypto-js");
const Order = require(`./../models/Order`);
const req = require("express/lib/request");

// REGISTER A USER
module.exports.register = async (reqBody) => {
    const {firstName, lastName, email, password, confirmPw} = reqBody

    const newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString(),
        confirmPw: CryptoJS.AES.encrypt(confirmPw, process.env.SECRET_PASS).toString()
    })
    return await User.findOne({email: email}).then((result, err) =>{
		if(result == null){
			if (reqBody.password === reqBody.confirmPw){
                return newUser.save()
                .then((result, err) => {
                    if (result){
                        return ( `You have successfully registered!`)
                    } else {
                        return err
                    }
                })
            } else {
                if (reqBody.password !== reqBody.confirmPw){
                    return (`Those passwords do not match. Please try again.`)
                }
            }
		} else {
			if(result != null){
				return (`Email already exists, please use a different one.`)
			} else {
				return err
			}
		}
	})
}

// GET ALL USERS
module.exports.getAllUsers = async () => {
    return await User.find().then(result => result)
}

// LOGIN A USER
module.exports.login = async (reqBody) => {
    return await User.findOne({email: reqBody.email}).then((result, err) => {
        if(result == null){
            return (`User does not exist`)
        } else {
           if (result !== null){
               const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

               if(reqBody.password == decryptedPw) {
                   return {token: createToken(result)}
                   
               } else {
                   return {auth: `Authentication Failed!`}
               }
            //    return (`You have logged in succesfully!`)
           } else {
               return err
           }
        }
    })
}

// RETRIEVE USER INFO 
module.exports.profile = async (userId) => {
    return await User.findById(userId).then((result,err) => {
        if (result){
            return result
        } else {
            if(result == null){
                return (`User does not exist.`)
            } else {
                return err
            }
        }
    })
}

// UPDATE USER INFO
module.exports.profileUpdate = async (reqBody) => {
    const userData = {
        email: reqBody.email,
        oldPassword: reqBody.oldPassword,
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString(),
        confirmPw: CryptoJS.AES.encrypt(reqBody.confirmPw, process.env.SECRET_PASS).toString()
    }


    return await User.findOneAndUpdate({email: reqBody.email}, {$set: userData}, {new:true}).then((result,err) => {
        if(result == null){
            return (`Email does not exist.`)
        } else {
            if(result !== null){
                const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
                if(reqBody.oldPassword == decryptedPw)
                    result.password = `*****`
                    result.confirmPw = `*****`
                    return result
            } else {
                return err
            }
        }
    })
}

// UPDATE USER'S ISADMIN STATUS TO TRUE
module.exports.adminStatus = async (userId) => {
    return await User.findByIdAndUpdate(userId, {$set: {isAdmin: true}}, {new: true}).then((result,err) => {
        if(result){
            return result 
        } else {
            if(result == null){
                return (`User is already an admin.`)
            } else {
                return err
            }
        }
    })
}

// UPDATE USER'S ISADMIN STATUS TO FALSE
module.exports.userStatus = async (userId) => {
    return await User.findByIdAndUpdate(userId, {$set: {isAdmin: false}}, {new: true}).then((result,err) => {
        if(result){
            return result 
        } else {
            if(result == null){
                return (`User is not yet an admin.`)
            } else {
                return err
            }
        }
    })
}

// DELETE A USER
module.exports.deleteUser = async (userId) => {

    return await User.findByIdAndDelete(userId).then((result,err) => {
        // result ? true :err
        if(result){
            return (`You have successfully deleted the user!`)
        } else{
            return err
        }
    })
}


