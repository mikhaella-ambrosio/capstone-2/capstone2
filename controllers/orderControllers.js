const Order = require(`./../models/Order`)
const Product = require('./../models/Product');
const CryptoJS = require("crypto-js"); 
const User = require('../models/User');
const res = require('express/lib/response');


// USER CHECKOUT
module.exports.checkout = async(data) =>{

    const {userId, productId, name, price, quantity} = data
    // console.log(data)

    const getAProduct = await Product.findById(productId).then((result, err) => {
        // console.log(result)
        if(result){
            return result
        }
        else{
            return err
        }
    })

    const newOrder = Order({
        userId: userId,
        productOrdered:[{
            productId: productId,
            name: name,
            price: price,
            quantity: quantity
        }],
        totalAmount: (getAProduct.price * quantity),
    })

    // console.log(newOrder)

    return await newOrder.save().then(result => {
        if(result){
            return true
        }
        else{
            if (result == null){
                return false
            }
        }
    })

}


// GET ALL ORDERS
module.exports.getAllOrders = async () => {
    return await Order.find().then(result => result)
}

// GET ALL PENDING ORDERS
module.exports.getAllPending = async () => {
    return await Order.find({orderStatus: "Pending"}).then(result => result)
}

// GET ALL DELIVERED ORDERS
module.exports.getAllDelivered = async () => {
    return await Order.find({orderStatus: "Delivered"}).then(result => result)
}

// GET USER'S ORDER/S
module.exports.getOrder = async (userId) => {

	return await Order.find({userId: userId}).then((result, err) => {
        // console.log(result)
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `User does not have an existing order. Please try seomething else.`}
			}else{
				return err
			}
		}
	})
}

// UPDATE USER'S ORDER STATUS STATUS TO DELIVERED
module.exports.delivered = async (reqBody) => {
    const {orderId} = reqBody
    return await Order.findByIdAndUpdate(orderId, {$set: {orderStatus: "Delivered"}}, {new: true}).then((result,err) => {
        if(result){
            return result
            // (`You have succesfully changed the order status of order ID: ${orderId}`)
        } else {
            return err
        }
    })
}

// DELETE A USER
module.exports.deleteOrder = async (reqBody) => {
    const {orderId} = reqBody

    return await Order.findByIdAndDelete(orderId).then((result,err) => {
        // result ? true :err
        if(result !== null){
            return (`Successfully deleted order!`)
        } else{
            if( result == null){
                return (`Order does not exist`)
            } else {
                return err
            }
        }
    })
}