const Product = require('./../models/Product');
const CryptoJS = require("crypto-js");
const { resetWatchers } = require('nodemon/lib/monitor/watch');

//CREATE A NEW PRODUCT
module.exports.create = async (reqBody) => {
	const {name, description, price} = reqBody

	let newProduct = new Product({
		name: name,
		description: description,
		price: price
	})

	return await newProduct.save().then((result, err) => {
        result ? result : err
        if(result){
            if(result !== null){
                return (`You have succesfully added a new product!`)
            } else {
                return (`Something went wrong. Please try again.`)
            }
        } else {
            return err
        }
    })
}

// GET ACTIVE PRODUCTS
module.exports.getActiveProducts = async () => {
    return await Product.find({isActive: "true"}).then(result => result)
}

// GET ALL PRODUCTS
module.exports.getAllProducts = async () => {
    return await Product.find().then(result => result)
}

// GET A SPECIFIC PRODUCT
module.exports.getAProduct = async (id) => {

	return await Product.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Product not found. Please try seomething else.`}
			}else{
				return err
			}
		}
	})
}

// GET ARCHIVED PRODUCTS
module.exports.getArchivedProducts = async () => {
    return await Product.find({isActive: "false"}).then(result => result)
}

// UPDATE A SPECIFIC PRODUCT
module.exports.updateProduct = async (productId, reqBody) => {
    let productData =  {
        name: reqBody.name, 
        description: reqBody.description, 
        price: reqBody.price
    }
	return await Product.findByIdAndUpdate(productId, {$set: productData}, {new: true}).then(result => {
        // console.log(result)
        // res.send(result)
        if(result !== null){
            // return {message: `You have successfully updated the product.`}
            return {result}
        } else {
            return {message: `Failed to update product. Try again.`}
        }
    })
}

// ARCHIVE A PRODUCT
module.exports.archive = async (productId) => {

	return await Product.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new:true}).then(result => result)
}

// UNARCHIVE A PRODUCT
module.exports.unarchive = async (productId) => {

	return await Product.findByIdAndUpdate(productId, {$set: {isActive: true}}, {new:true}).then(result => result)
}

// DELETE A PRODUCT
module.exports.deleteProduct = async (productId) => {

    return await Product.findByIdAndDelete(productId).then((result,err) => {
        // result ? true :err
        if(result){
            return (`You have successfully deleted the product.`)
        } else{
            return err
        }
    })
}
